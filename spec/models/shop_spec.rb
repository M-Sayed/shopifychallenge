require 'rails_helper'

RSpec.describe Shop, type: :model do
  before(:each) do
    @shop = FactoryBot.create(:shop)
  end

  context 'model design' do
    it 'responds to name' do
      expect(@shop).to respond_to(:name)
    end

    it 'responds to address' do
      expect(@shop).to respond_to(:address)
    end

    it 'responds to details' do
      expect(@shop).to respond_to(:details)
    end

    context "Associations" do
      it 'has many products' do
        expect(Shop.reflect_on_association(:products).macro).to eq(:has_many)
      end

      it 'has many orders' do
        expect(Shop.reflect_on_association(:orders).macro).to eq(:has_many)
      end
    end

  end

  context 'valid record' do
    it 'is valid when name is exist' do
      expect(@shop.name).to_not be_empty
      expect(@shop.name).to_not be_nil
      expect(@shop).to be_valid
    end
  end

  context 'invalid records' do
    it 'is invalid when name is blank' do
      @shop.name = ''
      expect(@shop.name).to be_empty
      expect(@shop).to be_invalid
    end

    it 'is invalid with a duplicate name' do
      persisted_shop = create(:shop)
      @shop.name = persisted_shop.name
      expect(@shop.name).to_not be_empty
      expect(@shop.name).to_not be_nil
      expect(@shop).to be_invalid
    end
  end
end
