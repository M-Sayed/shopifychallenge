require 'rails_helper'

RSpec.describe LineItem, type: :model do
  before(:each) do
    @line_item = build(:line_item)
  end

  context 'model design' do
    it 'should respond to quantity' do
      expect(@line_item).to respond_to(:quantity)
    end

    it 'should respond to product_id' do
      expect(@line_item).to respond_to(:product_id)
    end

    it 'should respond to order_id' do
      expect(@line_item).to respond_to(:order_id)
    end

    context "Associations" do
      it 'belongs to order' do
        expect(LineItem.reflect_on_association(:order).macro).to eq(:belongs_to)
      end

      it 'belongs to product' do
        expect(LineItem.reflect_on_association(:product).macro).to eq(:belongs_to)
      end
    end
  end

  context 'valid records' do
    it 'is valid when its order exist' do
      @order = FactoryBot.create(:order, shop: @line_item.product.shop,
                                         line_items: [@line_item])
      expect(@line_item.product).to_not be_nil
      expect(@line_item.order).to_not be_nil
      expect(@line_item).to_not be_new_record
      expect(@line_item).to be_valid
    end
  end

  context 'invalid records' do
    it 'is invalid without order' do
      expect(@line_item).to be_invalid
    end

    it 'is invalid when product does not exist' do
      @order = FactoryBot.create(:order, shop: @line_item.product.shop,
                                         line_items: [@line_item])
      @line_item.product = nil
      expect(@line_item).to be_invalid
    end
  end
end
