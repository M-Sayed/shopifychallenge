require 'rails_helper'

RSpec.describe Order, type: :model do
  before(:each) do
    3.times do
      shop = create(:shop)
      line_items = build_list(:line_item, 2, order_shop: shop)
      @order = create(:order, line_items: line_items,
                              shop: shop)
    end
  end

  context 'model design' do
    it 'should respond to total_price' do
      expect(@order).to respond_to(:total_price)
    end

    it 'should respond to shop_id' do
      expect(@order).to respond_to(:shop_id)
    end

    it 'should respond to products' do
      expect(@order).to respond_to(:products)
    end

    it 'should respond to line_items' do
      expect(@order).to respond_to(:line_items)
    end

    context "Associations" do
      it 'belongs to shop' do
        expect(Order.reflect_on_association(:shop).macro).to eq(:belongs_to)
      end

      it 'has many line items' do
        expect(Order.reflect_on_association(:line_items).macro).to eq(:has_many)
      end

      it 'has many products' do
        expect(Order.reflect_on_association(:products).macro).to eq(:has_many)
      end
    end
  end

  context 'valid records' do
    it 'is valid when total_price & shop exist' do
      expect(@order.shop).to_not be_nil
      expect(@order.total_price).to_not be_nil
      expect(@order.total_price).to be > 0
      expect(@order).to be_valid
    end

    it 'total_price equals sum of line_items total prices' do
      line_items = @order.line_items.pluck(:quantity)
      products   = @order.products.pluck(:price)
      expect(products.count).to be == (line_items.count)

      items = line_items.map.with_index { |_, i| [line_items[i], products[i]] }
      sum = items.reduce(0) { |sum, item| sum + item.reduce(1, :*) }
      expect(@order.total_price).to eq(sum)
    end

    it "total_price equals sum of line_items total prices after any line_item change" do
      @order.line_items.first.quantity += 1
      @order.save
      @order.reload

      line_items = @order.line_items.pluck(:quantity)
      products   = @order.products.pluck(:price)
      expect(products.count).to be == (line_items.count)

      items = line_items.map.with_index { |_, i| [line_items[i], products[i]] }
      sum = items.reduce(0) { |sum, item| sum + item.reduce(1, :*) }
      expect(@order.total_price).to eq(sum)
    end
  end

  context 'invalid records' do
    it 'is invalid when shop does not exist' do
      @order.shop = nil
      expect(@order).to be_invalid
    end

    it 'is invalid when total_price does not exist' do
      @order.total_price = nil
      expect(@order.total_price).to be_nil
      expect(@order).to be_invalid
    end

    it 'is invalid when there is no line items' do
      @order.line_items.destroy_all
      expect(@order.total_price).to eq(0)
      expect(@order).to be_invalid
    end

    it "is invalid if any line item's product doesn't belong to its shop" do
      rand_shop = Shop.where.not(id: @order.shop_id).first
      @order.products.first.update shop: rand_shop
      expect(@order).to be_invalid
    end
  end
end
