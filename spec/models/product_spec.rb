require 'rails_helper'

RSpec.describe Product, type: :model do
  before(:each) do
    @product = create(:product)
  end

  context 'model design' do
    it 'responds to price' do
      expect(@product).to respond_to(:price)
    end

    it 'responds to shop_id' do
      expect(@product).to respond_to(:shop_id)
    end

    context "Associations" do
      it 'belongs to shop' do
        expect(Product.reflect_on_association(:shop).macro).to eq(:belongs_to)
      end

      it 'has many line items' do
        expect(Product.reflect_on_association(:line_items).macro).to eq(:has_many)
      end

      it 'has many orders' do
        expect(Product.reflect_on_association(:orders).macro).to eq(:has_many)
      end
    end
  end

  context 'valid records' do
    it 'is valid when price & shop exist' do
      expect(@product.price).to_not be_nil
      expect(@product.shop).to_not be_nil
      expect(@product).to be_valid
    end
  end

  context 'invalid records' do
    it 'is invalid with no price' do
      @product.price = nil
      expect(@product).to be_invalid
    end

      it 'is invalid with no shop' do
        @product.shop = nil
      expect(@product).to be_invalid
    end
  end
end
