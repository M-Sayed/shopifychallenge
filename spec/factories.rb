FactoryBot.define do
  factory :shop, class: Shop do
    name    { Faker::Name.unique.name }
    address { Faker::Address.full_address }
    details { Faker::Lorem.paragraph }
  end

  factory :product, class: Product do
    name  { Faker::Name.unique.name }
    price { Faker::Number.decimal(3, 2) }
    shop
  end

  factory :order, class: Order do
    total_price { 0 }
    shop
    trait :line_items
  end

  factory :line_item, class: LineItem do
    transient do
      order_shop { FactoryBot.create(:shop) }
    end
    quantity { Faker::Number.between(1, 10) }
    product { FactoryBot.create(:product, shop: order_shop) }
  end
end
