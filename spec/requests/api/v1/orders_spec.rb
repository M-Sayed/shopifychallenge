require 'rails_helper'

RSpec.describe "Orders", type: :request do
  let(:headers) { { 'Content-Type' => 'application/json' } }

  let!(:shop) { create(:shop) }
  let!(:shop1) { create(:shop) }

  before { create_list(:product, 5, shop: shop1) }

  let!(:orders) do
    line_items = build_list(:line_item, 5, order_shop: shop)
    order = create(:order, shop: shop, line_items: line_items)

    line_items = build_list(:line_item, 5, order_shop: shop)
    order1 = create(:order, shop: shop, line_items: line_items)

    [order, order1]
  end

  let!(:order_id) { shop.orders.first.id }

  describe "GET shops/:shop_id/orders" do
    before { get api_v1_shop_orders_path(shop), headers: headers }

    it "works! responds ok - 200" do
      expect(response).to have_http_status(:ok)
    end

    it "returns all order of specific shop" do
      results = get_results(:orders)

      expect(results).not_to be_empty
      expect(results.size).to eq(2)
      expect(results[0][:line_items].size).to eq(5)
    end
  end

  describe "GET shops/:shop_id/orders/:id" do
    context 'Existing record' do
      before do
        get api_v1_shop_order_path(id: order_id, shop_id: shop.id), headers: headers
      end

      it 'responds with order json object' do
        result = get_results(:order)

        expect(result).to_not be_empty
        expect(result[:id]).to eq(order_id)
        expect(result[:line_items].class).to be Array
        expect(result[:shop]).to_not be_empty
      end

      it 'works! responds ok - 200' do
        expect(response).to have_http_status(:ok)
      end
    end

    context 'Non existing record' do
      before do
        max_order_id = shop.orders.maximum(:id)
        get api_v1_shop_order_path(id: max_order_id + 1, shop_id: shop.id), headers: headers
      end

      it 'works! responds not_found - 404' do
        expect(response).to have_http_status(:not_found)
      end

      it 'only gets its own orders' do
        get api_v1_shop_order_path(id: order_id, shop_id: shop1.id), headers: headers
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe "POST shops/:shop_id/orders" do
    context 'valid request' do
      before do
        line_items_attributes = []

        3.times do
          p = FactoryBot.create(:product, shop: shop)
          line_items_attributes.push({ product_id: p.id, quantity: rand(1..10) })
        end

        order_params = {
          line_items_attributes: line_items_attributes
        }
        post "/api/v1/shops/#{shop.id}/orders", params: { order: order_params }.to_json,
                                                headers: headers
      end

      it "works! responds created - 201" do
        expect(response).to have_http_status(:created)
      end

      it "returns the order json object" do
        result = get_results(:order)

        expect(result).to_not be_empty
      end
    end

    context 'invalid request' do
      before do
        order_params = FactoryBot.attributes_for(:order, line_item: [])

        post "/api/v1/shops/#{shop.id}/orders", params: { order: order_params }.to_json,
                                                headers: headers
      end

      it "works! responds unporcessable_entity - 422" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "responds with errors json object" do
        result = get_results(:errors)

        expect(result).not_to be_empty
      end
    end
  end

  describe "PUT/PATCH shops/:shop_id/orders" do
    context 'valid request' do
      before do
        line_items_attributes = []
        p = FactoryBot.create(:product, shop: shop)
        line_items_attributes.push({ product_id: p.id, quantity: rand(1..10) })

        li = shop.orders.first.line_items.first

        line_items_attributes.push({ id: li.id,
                                     product_id: li.product.id,
                                     quantity: li.quantity + 1 })

        order_params = {
          line_items_attributes: line_items_attributes
        }

        put "/api/v1/shops/#{shop.id}/orders/#{order_id}",
            params: { order: order_params }.to_json,
            headers: headers
      end

      it "works! responds no_content - 204" do
        expect(response).to have_http_status(:no_content)
      end
    end

    context 'invalid request' do
      before do
        line_items_attributes = [{ product_id: shop1.products.first.id,
                                   quantity: rand(1..10) }]

        order_params = {
          line_items_attributes: line_items_attributes
        }

        put "/api/v1/shops/#{shop.id}/orders/#{order_id}",
            params: { order: order_params }.to_json,
            headers: headers
      end

      it "works! responds unporcessable_entity - 422" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "responds with errors json object" do
        result = get_results(:errors)

        expect(result).not_to be_empty
      end
    end
  end

  describe "DELETE shops/:shop_id/orders/:id" do
    context 'valid request' do
      it "works! responds no_content - 204" do
        delete "/api/v1/shops/#{shop.id}/orders/#{order_id}", headers: headers
        expect(response).to have_http_status(:no_content)
      end

      it 'deletes all line items of the order, too' do
        li_ids = LineItem.where(order_id: order_id).pluck(:id)
        delete "/api/v1/shops/#{shop.id}/orders/#{order_id}", headers: headers
        expect(LineItem.where(id: li_ids).count).to eq(0)
      end
    end

    context 'invalid request' do
      it "works! responds not_found - 404" do
        max_order_id = shop.orders.maximum(:id)
        delete "/api/v1/shops/#{shop.id}/orders/#{max_order_id + 1}", headers: headers
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
