require 'rails_helper'

RSpec.describe "Shops", type: :request do
  let(:headers) { { 'Content-Type' => 'application/json' } }

  let!(:shops) { create_list(:shop, 15) }
  let!(:shop_id) { shops.first.id }

  describe "GET /shops" do
    before { get api_v1_shops_path, headers: headers }

    it "works! responds ok - 200" do
      expect(response).to have_http_status(:ok)
    end

    it "returns all shops" do
      results = get_results(:shops)

      expect(results).not_to be_empty
      expect(results.size).to eq(15)
    end
  end

  describe "GET /shops/:id" do
    context 'Existing record' do
      before do
        get "/api/v1/shops/#{shop_id}", headers: headers
      end

      it 'responds with shop json object' do
        result = get_results(:shop)

        expect(result).to_not be_empty
        expect(result[:id]).to eq(shop_id)
        expect(result[:orders].class).to be Array
        expect(result[:products].class).to be Array
      end

      it 'works! responds ok - 200' do
        expect(response).to have_http_status(:ok)
      end
    end

    context 'Non existing record' do
      before do
        max_shop_id = Shop.maximum(:id)
        get "/api/v1/shops/#{max_shop_id + 1}", headers: headers
      end

      it 'works! responds not_found - 404' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe "POST /shops" do
    context 'valid request' do
      before do
        post '/api/v1/shops', params: { shop: FactoryBot.attributes_for(:shop) }.to_json,
                              headers: headers
      end

      it "works! responds created - 201" do
        expect(response).to have_http_status(:created)
      end

      it "returns the shop json object" do
        result = get_results(:shop)

        expect(result).to_not be_empty
      end
    end

    context 'invalid request' do
      before do
        shop_params = FactoryBot.attributes_for(:shop)
        shop_params.delete(:name)
        post '/api/v1/shops', params: { shop: shop_params }.to_json, headers: headers
      end

      it "works! responds unporcessable_entity - 422" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "responds with errors json object" do
        result = get_results(:errors)

        expect(result).not_to be_empty
      end
    end
  end

  describe "PUT/PATCH /shops/:id" do
    context 'valid request' do
      before do
        put "/api/v1/shops/#{shop_id}", params: {
                                          shop: { name: Faker::Name.unique.name }
                                        }.to_json,
                                        headers: headers
      end

      it "works! responds no_content - 204" do
        expect(response).to have_http_status(:no_content)
      end
    end

    context 'invalid request' do
      before do
        put "/api/v1/shops/#{shop_id}", params: {
                                          shop: { name: Shop.last.name }
                                        }.to_json,
                                        headers: headers
      end

      it "works! responds unporcessable_entity - 422" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "responds with errors json object" do
        result = get_results(:errors)

        expect(result).not_to be_empty
      end
    end
  end

  describe "DELETE /shops/:id" do
    context 'valid request' do
      it "works! responds no_content - 204" do
        delete "/api/v1/shops/#{shop_id}", headers: headers
        expect(response).to have_http_status(:no_content)
      end
    end

    context 'invalid request' do
      it "works! responds not_found - 404" do
        max_shop_id = Shop.maximum(:id)
        delete "/api/v1/shops/#{max_shop_id + 1}", headers: headers
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
