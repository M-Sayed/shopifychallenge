require 'rails_helper'

RSpec.describe "Api::V1::LineItems", type: :request do
  let(:headers) { { 'Content-Type' => 'application/json' } }
  
  let!(:shop) { create(:shop) }

  let!(:order) do
    line_items = build_list(:line_item, 5, order_shop: shop)
    order = create(:order, shop: shop, line_items: line_items)
  end

  let!(:line_item) { order.line_items.first }
  let!(:line_item_id) { line_item.id }

  describe "GET /shops/:shop_id/orders/:order_id/line_items" do
    before { get api_v1_shop_order_line_items_path(shop, order), headers: headers }

    it "works! responds ok - 200" do
      expect(response).to have_http_status(:ok)
    end

    it "returns all line_items of specific order & shop" do
      results = get_results(:line_items)

      expect(results).not_to be_empty
      expect(results.size).to eq(5)
    end
  end

  describe "GET /shops/:shop_id/orders/:order_id/line_items/:id" do
    context 'Existing record' do
      before do
        get api_v1_shop_order_line_item_path(shop, order, line_item), headers: headers
      end

      it 'responds with line_item json object' do
        result = get_results(:line_item)

        expect(result).to_not be_empty
        expect(result[:id]).to eq(line_item_id)
      end

      it 'works! responds ok - 200' do
        expect(response).to have_http_status(:ok)
      end
    end

    context 'Non existing record' do
      before do
        max_line_item_id = order.line_items.maximum(:id)
        get api_v1_shop_order_line_item_path(shop, order, id: max_line_item_id + 1), headers: headers
      end

      it 'works! responds not_found - 404' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe "POST shops/:shop_id/orders/:order_id/line_items" do
    context 'valid request' do
      before do
        line_item_params = {
          quantity: Faker::Number.between(1, 10),
          product_id: FactoryBot.create(:product, shop: shop).id
        }
        post "/api/v1/shops/#{shop.id}/orders/#{order.id}/line_items",
          params: { line_item: line_item_params }.to_json,
          headers: headers
      end

      it "works! responds created - 201" do
        expect(response).to have_http_status(:created)
      end

      it "returns the product json object" do
        result = get_results(:line_item)

        expect(result).to_not be_empty
      end
    end

    context 'invalid request' do
      before do
        line_item_params = {
          quantity: 0,
          product_id: FactoryBot.create(:product, shop: shop).id
        }
        post "/api/v1/shops/#{shop.id}/orders/#{order.id}/line_items",
          params: { line_item: line_item_params }.to_json,
          headers: headers
      end

      it "works! responds unporcessable_entity - 422" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "responds with errors json object" do
        result = get_results(:errors)

        expect(result).not_to be_empty
      end
    end
  end

  describe "PUT/PATCH shops/:shop_id/orders/:order_id/line_items/:id" do
    context 'valid request' do
      before do
        line_item_params = {
          quantity: Faker::Number.between(1, 10)
        }
        put "/api/v1/shops/#{shop.id}/orders/#{order.id}/line_items/#{line_item_id}",
              params: { line_item: line_item_params }.to_json,
              headers: headers
      end

      it "works! responds no_content - 204" do
        expect(response).to have_http_status(:no_content)
      end
    end

    context 'invalid request' do
      before do
        line_item_params = {
          quantity: -1
        }
        put "/api/v1/shops/#{shop.id}/orders/#{order.id}/line_items/#{line_item_id}",
              params: { line_item: line_item_params }.to_json,
              headers: headers
      end

      it "works! responds unporcessable_entity - 422" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "responds with errors json object" do
        result = get_results(:errors)

        expect(result).not_to be_empty
      end
    end
  end

  describe "DELETE /shops/:shop_id/orders/:order_id/line_items/:id" do
    context 'valid request' do
      it "works! responds no_content - 204" do
        delete "/api/v1/shops/#{shop.id}/orders/#{order.id}/line_items/#{line_item_id}", headers: headers
        expect(response).to have_http_status(:no_content)
      end

      it 'deletes the order if no line items left.' do
        4.times { order.line_items.first.destroy; order.reload }

        order_id = order.id
        line_item_id = order.line_items.first.id

        delete "/api/v1/shops/#{shop.id}/orders/#{order.id}/line_items/#{line_item_id}", headers: headers

        expect(response).to have_http_status(:no_content)
        expect(Order.exists?(order_id)).to eq(false)
      end
    end

    context 'invalid request' do
      it "works! responds not_found - 404" do
        puts order.inspect
        max_line_item_id = order.line_items.maximum(:id) + 1
        delete "/api/v1/shops/#{shop.id}/orders/#{order.id}/line_items/#{max_line_item_id}", headers: headers
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
