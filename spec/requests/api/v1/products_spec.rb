require 'rails_helper'

RSpec.describe "Api::V1::Products", type: :request do
  let(:headers) { { 'Content-Type' => 'application/json' } }

  let!(:shop) { create(:shop) }

  let!(:products) { create_list(:product, 3, shop: shop) }
  let!(:product_id) { products.first.id }

  let!(:order) do
    line_items = build_list(:line_item, 2, order_shop: shop)
    order = create(:order, shop: shop, line_items: line_items)
  end

  describe "GET shops/:shop_id/orders/:order_id/products" do
    before { get api_v1_shop_order_products_path(shop, order), headers: headers }

    it "works! responds ok - 200" do
      expect(response).to have_http_status(:ok)
    end

    it "returns all products of specific order & shop" do
      results = get_results(:products)

      expect(results).not_to be_empty
      expect(results.size).to eq(2)
    end
  end

  describe "GET shops/:shop_id/products" do
    before { get api_v1_shop_products_path(shop), headers: headers }

    it "works! responds ok - 200" do
      expect(response).to have_http_status(:ok)
    end

    it "returns all products of specific shop" do
      results = get_results(:products)

      expect(results).not_to be_empty
      expect(results.size).to eq(5)
    end
  end

  describe "GET shops/:shop_id/products/:id" do
    context 'Existing record' do
      before do
        get api_v1_shop_product_path(id: product_id, shop_id: shop.id), headers: headers
      end

      it 'responds with product json object' do
        result = get_results(:product)

        expect(result).to_not be_empty
        expect(result[:id]).to eq(product_id)
      end

      it 'works! responds ok - 200' do
        expect(response).to have_http_status(:ok)
      end
    end

    context 'Non existing record' do
      before do
        max_product_id = shop.products.maximum(:id)
        get api_v1_shop_product_path(id: max_product_id + 1, shop_id: shop.id), headers: headers
      end

      it 'works! responds not_found - 404' do
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe "POST shops/:shop_id/products" do
    context 'valid request' do
      before do
        product_params = FactoryBot.attributes_for(:product)
        post "/api/v1/shops/#{shop.id}/products", params: { product: product_params }.to_json,
                                                  headers: headers
      end

      it "works! responds created - 201" do
        expect(response).to have_http_status(:created)
      end

      it "returns the product json object" do
        result = get_results(:product)

        expect(result).to_not be_empty
      end
    end

    context 'invalid request' do
      before do
        product_params = FactoryBot.attributes_for(:product)
        product_params[:name] = nil

        post "/api/v1/shops/#{shop.id}/products", params: { product: product_params }.to_json,
                                                  headers: headers
      end

      it "works! responds unporcessable_entity - 422" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "responds with errors json object" do
        result = get_results(:errors)

        expect(result).not_to be_empty
      end
    end
  end

  describe "PUT/PATCH shops/:shop_id/products/:id" do
    context 'valid request' do
      before do
        product_params = { name: Faker::Name.name }
        put "/api/v1/shops/#{shop.id}/products/#{product_id}",
            params: { product: product_params }.to_json,
            headers: headers
      end

      it "works! responds no_content - 204" do
        expect(response).to have_http_status(:no_content)
      end
    end

    context 'invalid request' do
      before do
        product_params = { name: nil }

        put "/api/v1/shops/#{shop.id}/products/#{product_id}",
            params: { product: product_params }.to_json,
            headers: headers
      end

      it "works! responds unporcessable_entity - 422" do
        expect(response).to have_http_status(:unprocessable_entity)
      end

      it "responds with errors json object" do
        result = get_results(:errors)

        expect(result).not_to be_empty
      end
    end
  end

  describe "DELETE shops/:shop_id/products/:id" do
    context 'valid request' do
      it "works! responds no_content - 204" do
        delete "/api/v1/shops/#{shop.id}/products/#{product_id}", headers: headers
        expect(response).to have_http_status(:no_content)
      end

      it 'deletes all line items of the product, too' do
        product_id = order.products.first.id # make sure it has line items.
        li_ids = LineItem.where(product_id: product_id).pluck(:id)
        assert(li_ids.count > 0)             # make sure it has line items.
        delete "/api/v1/shops/#{shop.id}/products/#{product_id}", headers: headers
        expect(LineItem.where(id: li_ids).count).to eq(0)
      end
    end

    context 'invalid request' do
      it "works! responds not_found - 404" do
        max_product_id = shop.products.maximum(:id)
        delete "/api/v1/shops/#{shop.id}/products/#{max_product_id + 1}", headers: headers
        expect(response).to have_http_status(:not_found)
      end
    end
  end
end
