class Rack::Attack
  Rack::Attack.safelist("mark any authenticated access safe") do |request|
    request.env['x-api-key'] == ENV['x-api-key']
  end

  Rack::Attack.safelist('allow from localhost') do |req|
    '127.0.0.1' == req.ip || '::1' == req.ip
  end

  throttle('request by ip', limit: 5, period: 1) do |req|
    req.ip
  end
end
