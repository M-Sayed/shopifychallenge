Rails.application.routes.draw do
  namespace :api,
    constraints: lambda { |req| req.headers['Content-Type'] == 'application/json' } do
    namespace :v1 do

      resources :shops do
        resources :orders do
          resources :products, only: [:index]
          resources :line_items
        end
        resources :products
      end
    end
  end

  post '/graphql', to: 'graphql#execute'
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: 'graphql'
  end
end
