class Order < ApplicationRecord
  # Validaitons
  validates :line_items, length: { :minimum => 1 }
  validates :total_price, presence: true

  # Callbacks
  before_save       :calculate_total_price
  before_validation :assert_products_property

  # Associations
  belongs_to :shop
  has_many :line_items, dependent: :destroy
  has_many :products, through: :line_items

  accepts_nested_attributes_for :line_items

  private

  def calculate_total_price
    self.total_price = line_items.sum_prices
  end

  def assert_products_property
    products_shop_ids = products.pluck(:shop_id).uniq

    return if products_shop_ids.empty?
    return if products_shop_ids.length == 1 && shop_id == products_shop_ids.first

    errors.add(:products, 'should belong to the same shop.')
  end
end
