class LineItem < ApplicationRecord
  delegate :price, to: :product

  # Scopes
  scope :sum_prices, -> {
    joins(:product).sum('line_items.quantity * products.price')
  }

  # Validations
  validate :product_shop
  validates :quantity,
            numericality: { greater_than: 0 }, on: :create
  validates :quantity,
            numericality: { greater_than_or_equal_to: 0 },
            on: :update

  # Callbacks
  after_commit  :update_total_price
  after_destroy :destroy_order_with_no_items
  after_update  :delete_zero_quantity

  # Associations
  belongs_to :product
  belongs_to :order

  private

  def product_shop
    return if order && product && order.shop_id == product.shop_id
    unless order && product
      errors.add(:product, "product & product should both exist.")
    else
      errors.add(:product, "'#{product.name}' belongs to shop '#{product.shop.name}'.")
    end
  end

  def update_total_price
    if order && order.persisted?
      order.update_attributes(total_price: order.line_items.sum_prices)
    end
  end

  def destroy_order_with_no_items
    if order && order.persisted? && order.line_items.count == 0
      order.destroy
    end
  end

  def delete_zero_quantity
    quantity == 0 && destroy
  end
end
