class Shop < ApplicationRecord
  validates :name, presence: true, uniqueness: true

  has_many :products, dependent: :destroy
  has_many :orders, dependent: :destroy
end
