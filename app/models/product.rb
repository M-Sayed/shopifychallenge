class Product < ApplicationRecord
  scope :of_order, -> (order_id) { where(order_id: order_id) }

  validates :price, :name, presence: true

  belongs_to :shop
  has_many :line_items, dependent: :destroy
  has_many :orders, through: :line_items
end
