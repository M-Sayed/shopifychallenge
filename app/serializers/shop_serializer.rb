class ShopSerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :details

  has_many :orders
  has_many :products
end
