class LineItemSerializer < ActiveModel::Serializer
  attributes :id, :quantity, :unit_price, :product_name


  def unit_price
    object.product.price
  end

  def product_name
    object.product.name
  end
end
