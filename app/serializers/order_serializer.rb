class OrderSerializer < ActiveModel::Serializer
  attributes :id, :total_price

  belongs_to :shop
  has_many :line_items
end
