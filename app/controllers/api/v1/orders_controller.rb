## ### Orders Controller APIs
## #### Endpoints provided by this controller:
## - GET    /api/v1/shops/:shop_id/orders
## - POST   /api/v1/shops/:shop_id/orders
## - GET    /api/v1/shops/:shop_id/orders/:id
## - PATCH  /api/v1/shops/:shop_id/orders/:id
## - PUT    /api/v1/shops/:shop_id/orders/:id
## - DELETE /api/v1/shops/:shop_id/orders/:id
##
class Api::V1::OrdersController < ApplicationController
  before_action :set_shop
  before_action :set_order, only: [:show, :update, :destroy]

  ## #### `GET /api/v1/shops/:shop_id/orders`
  ## *Orders index to list all the orders of a shop.*
  ##
  ## >##### Request
  ## >>`GET /api/v1/shops/:shop_id/orders`
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >
  ## >##### Response
  ## >>###### body
  ## >>```json
  ## >>{
  ## >>  "orders": [{
  ## >>    "id": 1,
  ## >>    "total_price": "4108.25",
  ## >>    "shop": {
  ## >>      "id": 1,
  ## >>      "name": "Shop_0",
  ## >>      "address": "Apt. 929 3381 Lebsack Oval, South Ian, NC 82844-8016",
  ## >>      "details": "Est est qui. Ut animi quo. Aliquam nam placeat."
  ## >>    },
  ## >>    "line_items": [{
  ## >>      "id": 1,
  ## >>      "quantity": 1,
  ## >>      "unit_price": "504.01",
  ## >>      "product_name": "Jannet Gislason"
  ## >>    }, {
  ## >>      "id": 2,
  ## >>      "quantity": 4,
  ## >>      "unit_price": "901.06",
  ## >>      "product_name": "Zachery O'Keefe"
  ## >>    }]
  ## >>  }]
  ## >>}
  ## >>```
  def index
    render json: @shop.orders
  end

  ## #### `GET /api/v1/shops/:shop_id/orders/:id`
  ## *Get a specific order of a specific shop by id.*
  ##
  ## >##### Request
  ## >>
  ## >>`GET /api/v1/shops/:shop_id/orders/:id`
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >##### Response
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "order": {
  ## >>    "id": 1,
  ## >>    "total_price": "4108.25",
  ## >>    "shop": {
  ## >>      "id": 1,
  ## >>      "name": "Shop_0",
  ## >>      "address": "Apt. 929 3381 Lebsack Oval, South Ian, NC 82844-8016",
  ## >>      "details": "Est est qui. Ut animi quo. Aliquam nam placeat."
  ## >>    },
  ## >>    "line_items": [{
  ## >>      "id": 1,
  ## >>      "quantity": 1,
  ## >>      "unit_price": "504.01",
  ## >>      "product_name": "Jannet Gislason"
  ## >>    }, {
  ## >>      "id": 2,
  ## >>      "quantity": 4,
  ## >>      "unit_price": "901.06",
  ## >>      "product_name": "Zachery O'Keefe"
  ## >>    }]
  ## >>  }
  ## >>}
  ## ```
  def show
    render json: @order
  end

  ## #### `POST /api/v1/shops/:shop_id/orders`
  ## *Create new order.*
  ##
  ## >##### Request
  ## >>`POST /api/v1/shops/:shop_id/orders`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "order": {
  ## >>    "line_items_attributes": [{
  ## >>      "product_id": 1,
  ## >>      "quantity": 2
  ## >>    }, {
  ## >>      "product_id": 2,
  ## >>      "quantity": 4
  ## >>    }]
  ## >>  }
  ## >>}
  ## >>```
  ## >
  ## >##### Response
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "order": {
  ## >>    "id": 1,
  ## >>    "total_price": "4108.25",
  ## >>    "shop": {
  ## >>      "id": 1,
  ## >>      "name": "Shop_0",
  ## >>      "address": "Apt. 929 3381 Lebsack Oval, South Ian, NC 82844-8016",
  ## >>      "details": "Est est qui. Ut animi quo. Aliquam nam placeat."
  ## >>    },
  ## >>    "line_items": [{
  ## >>      "id": 1,
  ## >>      "quantity": 2,
  ## >>      "unit_price": "504.01",
  ## >>      "product_name": "Jannet Gislason"
  ## >>    }, {
  ## >>      "id": 2,
  ## >>      "quantity": 4,
  ## >>      "unit_price": "901.06",
  ## >>      "product_name": "Zachery O'Keefe"
  ## >>    }]
  ## >>  }
  ## >>}
  ## ```
  def create
    @order = @shop.orders.new orders_params
    if @order.save
      render json: @order, status: :created
    else
      render json: { errors: @order.errors }, status: :unprocessable_entity
    end
  end

  ## #### `PUT/PATCH /api/v1/shops/:shop_id/orders/:id`
  ## *update existing order by id*
  ##
  ## >##### Request
  ## >>`PUT/PATCH /api/v1/shops/:shop_id/orders/:id`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "order": {
  ## >>    "line_items_attributes": [{
  ## >>      "product_id": 1,
  ## >>      "quantity": 3
  ## >>    }]
  ## >>  }
  ## >>}
  ## >>```
  ## >
  ## >##### Response
  ## >> `204`
  def update
    if @order.update orders_params
      head :no_content
    else
      render json: { errors: @order.errors }, status: :unprocessable_entity
    end
  end

  ## #### `DELETE /api/v1/shops/:shop_id/orders/:id`
  ## *Delete a specific order by id*
  ##
  ## >##### Request
  ## >>`DELETE /api/v1/shops/:shop_id/orders/:id`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >##### Response
  ## >> `204`
  def destroy
    @order.destroy
  end

  private

  def set_shop
    @shop = Shop.find params[:shop_id]
  end

  def set_order
    @order = @shop.orders.find params[:id]
  end

  def orders_params
    params.require(:order)
          .permit(line_items_attributes: [:id, :quantity, :product_id])
  end
end
