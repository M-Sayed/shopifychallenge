## ### LineItems Controller APIs
## #### Endpoints provided by this controller:
## - GET    /api/v1/shops/:shop_id/orders/:order_id/line_items
## - POST   /api/v1/shops/:shop_id/orders/:order_id/line_items
## - GET    /api/v1/shops/:shop_id/orders/:order_id/line_items/:id
## - PATCH  /api/v1/shops/:shop_id/orders/:order_id/line_items/:id
## - PUT    /api/v1/shops/:shop_id/orders/:order_id/line_items/:id
## - DELETE /api/v1/shops/:shop_id/orders/:order_id/line_items/:id
##
class Api::V1::LineItemsController < ApplicationController
  before_action :set_order
  before_action :set_line_item, only: [:show, :update, :destroy]

  ## #### `GET /api/v1/shops/:shop_id/orders/:order_id/line_items`
  ## *LineItems index to list all the line items of an order.*
  ##
  ## >##### Request
  ## >>`GET /api/v1/shops/:shop_id/orders/:order_id/line_items`
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >
  ## >##### Response
  ## >>###### body
  ## >>```json
  ## >>{
  ## >>  "line_items": [{
  ## >>    "id": 1,
  ## >>    "quantity": 1,
  ## >>    "unit_price": "504.01",
  ## >>    "product_name": "Jannet Gislason"
  ## >>  }, {
  ## >>    "id": 2,
  ## >>    "quantity": 4,
  ## >>    "unit_price": "901.06",
  ## >>    "product_name": "Zachery O'Keefe"
  ## >>  }]
  ## >>}
  ## >>```
  def index
    render json: @order.line_items
  end

  ## #### `GET /api/v1/shops/:shop_id/orders/:order_id/line_items/:id`
  ## *Get a specific line item of an order.*
  ##
  ## >##### Request
  ## >>
  ## >>`GET /api/v1/shops/:shop_id/orders/:order_id/line_items/:id`
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >##### Response
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "line_item": {
  ## >>    "id": 1,
  ## >>    "quantity": 1,
  ## >>    "unit_price": "504.01",
  ## >>    "product_name": "Jannet Gislason"
  ## >>  }
  ## >>}
  ## >>```
  def show
    render json: @line_item
  end

  ## #### `POST /api/v1/shops/:shop_id/orders/:order_id/line_items`
  ## *Add new line item to an order.*
  ##
  ## >##### Request
  ## >>`POST /api/v1/shops/:shop_id/orders/:order_id/line_items`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "line_item": {
  ## >>    "product_id": 1,
  ## >>    "quantity": 2
  ## >>  }
  ## >>}
  ## >>```
  ## >
  ## >##### Response
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "line_item": {
  ## >>    "id": 1,
  ## >>    "quantity": 2,
  ## >>    "unit_price": "504.01",
  ## >>    "product_name": "Jannet Gislason"
  ## >>  }
  ## >>}
  ## ```
  def create
    @line_item = @order.line_items.new line_item_params(:on_create)
    if @line_item.save
      render json: @line_item, status: :created
    else
      render json: { errors: @line_item.errors }, status: :unprocessable_entity
    end
  end

  ## #### `PUT/PATCH /api/v1/shops/:shop_id/orders/:order_id/line_items/:id`
  ## *update line item quantity of an order.*
  ##
  ## >##### Request
  ## >>`PUT/PATCH /api/v1/shops/:shop_id/orders/:order_id/line_items/:id`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "line_item": {
  ## >>    "quantity": 5
  ## >>  }
  ## >>}
  ## >>```
  ## >
  ## >##### Response
  ## >> `204`
  def update
    if @line_item.update line_item_params
      head :no_content
    else
      render json: { errors: @line_item.errors }, status: :unprocessable_entity
    end
  end

  ## #### `DELETE /api/v1/shops/:shop_id/orders/:order_id/line_items/:id`
  ## *Delete a specific line item by id*
  ##
  ## >##### Request
  ## >>`DELETE /api/v1/shops/:id`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >##### Response
  ## >> `204`
  def destroy
    @line_item.destroy
  end

  private

  def set_order
    @order = Order.find params[:order_id]
  end

  def set_line_item
    @line_item = @order.line_items.find params[:id]
  end

  def line_item_params(on = :on_update)
    permited_params = [:quantity]
    permited_params << :product_id if on == :on_create

    params.require(:line_item)
          .permit(permited_params)
  end
end
