## ### Products Controller APIs
## #### Endpoints provided by this controller:
## - GET    /api/v1/shops/:shop_id/products
## - GET    /api/v1/shops/:shop_id/orders/:order_id/products
## - POST   /api/v1/shops/:shop_id/products
## - GET    /api/v1/shops/:shop_id/products/:id
## - PATCH  /api/v1/shops/:shop_id/products/:id
## - PUT    /api/v1/shops/:shop_id/products/:id
## - DELETE /api/v1/shops/:shop_id/products/:id
##
class Api::V1::ProductsController < ApplicationController
  before_action :set_shop
  before_action :set_product, only: [:show, :update, :destroy]
  before_action :set_products, only: [:index]

  ## #### `GET /api/v1/shops/:shop_id/products`
  ## *List all products of a specific shop.*
  ##
  ## >##### Request
  ## >>`GET /api/v1/shops/:shop_id/products`
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >
  ## >##### Response
  ## >>###### body
  ## >>```json
  ## >>{
  ## >>  "products": [{
  ## >>    "id": 1,
  ## >>    "name": "Emery Bauch",
  ## >>    "price": "807.09"
  ## >>  },
  ## >>  {
  ## >>    "id": 2,
  ## >>    "name": "Dr. Li Ryan",
  ## >>    "price": "607.04"
  ## >>  }]
  ## >>}
  ## >>```
  ##
  ##
  ## #### `GET /api/v1/shops/:shop_id/orders/:order_id/products`
  ## *List all products of a specific shop and order.*
  ##
  ## >##### Request
  ## >>`GET /api/v1/shops/:shop_id/orders/:order_id/products`
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >
  ## >##### Response
  ## >>###### body
  ## >>```json
  ## >>{
  ## >>  "products": [{
  ## >>    "id": 3,
  ## >>    "name": "Jannet Gislason",
  ## >>    "price": "504.01"
  ## >>  },
  ## >>  {
  ## >>    "id": 5,
  ## >>    "name": "Zachery O'Keefe",
  ## >>    "price": "901.06"
  ## >>  }]
  ## >>}
  ## >>```
  def index
    render json: @products
  end

  ## #### `GET /api/v1/shops/:shop_id/products/:id`
  ## *Get a specific prodyct of a shop by id.*
  ##
  ## >##### Request
  ## >>
  ## >>`GET /api/v1/shops/:shop_id/products/:id`
  ## >>###### headers
  ## >>>- Content-Type: application/json
  ## >>
  ## >##### Response
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "product": {
  ## >>    "id": 1,
  ## >>    "name": "Emery Bauch",
  ## >>    "price": "807.09"
  ## >>  }
  ## >>}
  ## ```
  def show
    render json: @product
  end

  ## #### `POST /api/v1/shops/:shop_id/products`
  ## *Create new product for a specific shop.*
  ##
  ## >##### Request
  ## >>`POST /api/v1/shops/:shop_id/products`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "product": {
  ## >>    "name": "Emery Bauch",
  ## >>    "price": "807.09"
  ## >>  }
  ## >>}
  ## >>```
  ## >
  ## >##### Response
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "product": {
  ## >>    "id": 1,
  ## >>    "name": "Emery Bauch",
  ## >>    "price": "807.09"
  ## >>  }
  ## >>}
  ## ```
  def create
    @product = @shop.products.new product_params
    if @product.save
      render json: @product, status: :created
    else
      render json: { errors: @product.errors }, status: :unprocessable_entity
    end
  end

  ## #### `PUT/PATCH /api/v1/shops/:shop_id/products/:id`
  ## *Update existing product by id*
  ##
  ## >##### Request
  ## >>`PUT/PATCH /api/v1/shops/:shop_id/products/:id`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "product": {
  ## >>    "name": "Emery Bauch new",
  ## >>  }
  ## >>}
  ## >>```
  ## >
  ## >##### Response
  ## >> `204`
  def update
    if @product.update product_params
      head :no_content
    else
      render json: { errors: @product.errors }, status: :unprocessable_entity
    end
  end

  # TODO: soft delete!!
  ## #### `DELETE /api/v1/shops/:shop_id/products/:id`
  ## *Delete a specific product by id*
  ##
  ## >##### Request
  ## >>`DELETE /api/v1/shops/:shop_id/products/:id`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >##### Response
  ## >> `204`
  def destroy
    @product.destroy
  end

  private

  def set_shop
    @shop = Shop.find params[:shop_id]
  end

  def set_product
    @product = @shop.products.find params[:id]
  end

  def product_params
    p_params = params.require(:product)
                     .permit(:name, :price)
    # TODO: any better way to do so?
    p_params[:price] &&= p_params[:price].try(:to_d).try(:truncate, 2)
    p_params
  end

  def set_products
    @products = if params[:order_id]
                  @shop.orders.find_by(id: params[:order_id]).products
                else
                  @shop.products
                end
  end
end
