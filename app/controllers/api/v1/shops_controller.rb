## ### Shops Controller APIs
## #### Endpoints provided by this controller:
## - GET    /api/v1/shops
## - POST   /api/v1/shops
## - GET    /api/v1/shops/:id
## - PATCH  /api/v1/shops/:id
## - PUT    /api/v1/shops/:id
## - DELETE /api/v1/shops/:id
##
class Api::V1::ShopsController < ApplicationController
  before_action :set_shop, only: [:show, :update, :destroy]

  ## #### `GET /api/v1/shops/`
  ## *Shops index to list all the shops.*
  ##
  ## >##### Request
  ## >>`GET /api/v1/shops/`
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >
  ## >##### Response
  ## >>###### body
  ## >>```json
  ## >>{
  ## >>  "shops": [{
  ## >>    "id": 1,
  ## >>    "name": "shop1",
  ## >>    "address": "egypt",
  ## >>    "details": "details",
  ## >>    "orders": [{
  ## >>      "id": 1,
  ## >>      "total_price": "203.74"
  ## >>    }],
  ## >>    "products": [{
  ## >>      "id": 1,
  ## >>      "name": "Shampoo",
  ## >>      "price": "19.99"
  ## >>    }]
  ## >>  }, {
  ## >>    "id": 2,
  ## >>    "name": "shop2",
  ## >>    "address": "egypt",
  ## >>    "details": "details",
  ## >>    "orders": [],
  ## >>    "products": [{
  ## >>      "id": 9,
  ## >>      "name": "blue logitech mouse shop3",
  ## >>      "price": "30.19"
  ## >>    }]
  ## >>  }]
  ## >>}
  ## >>```
  def index
    render json: Shop.all
  end

  ## #### `GET /api/v1/shops/:id`
  ## *Get a specific shop by id.*
  ##
  ## >##### Request
  ## >>
  ## >>`GET /api/v1/shops/:id`
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >##### Response
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "shop": {
  ## >>    "id": 1,
  ## >>    "name": "shop1",
  ## >>    "address": "egypt",
  ## >>    "details": "details",
  ## >>    "orders": [{
  ## >>      "id": 1,
  ## >>      "total_price": "203.74"
  ## >>    }],
  ## >>    "products": [{
  ## >>      "id": 1,
  ## >>      "name": "Shampoo",
  ## >>      "price": "19.99"
  ## >>    }]
  ## >>  }
  ## >>}
  ## ```
  def show
    render json: @shop
  end

  ## #### `POST /api/v1/shops`
  ## *Create new shop.*
  ##
  ## >##### Request
  ## >>`POST /api/v1/shops`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "shop": {
  ## >>    "name": "shop1",
  ## >>    "address": "egypt",
  ## >>    "details": "details"
  ## >>  }
  ## >>}
  ## >>```
  ## >
  ## >##### Response
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "shop": {
  ## >>    "id": 1,
  ## >>    "name": "shop1",
  ## >>    "address": "egypt",
  ## >>    "details": "details"
  ## >>  }
  ## >>}
  ## ```
  def create
    @shop = Shop.create(shop_params)
    if @shop.save
      render json: @shop, status: :created
    else
      render json: { errors: @shop.errors }, status: :unprocessable_entity
    end
  end

  ## #### `PUT/PATCH /api/v1/shops/:id`
  ## *Update existing shop by id*
  ##
  ## >##### Request
  ## >>`PUT/PATCH /api/v1/shops/:id`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >>###### body:
  ## >>```json
  ## >>{
  ## >>  "shop": {
  ## >>    "name": "shop2",
  ## >>    "details": "details2"
  ## >>  }
  ## >>}
  ## >>```
  ## >
  ## >##### Response
  ## >>`204`
  def update
    if @shop.update shop_params
      head :no_content
    else
      render json: { errors: @shop.errors }, status: :unprocessable_entity
    end
  end

  ## #### `DELETE /api/v1/shops/:id`
  ## *Delete a specific shop by id*
  ##
  ## >##### Request
  ## >>`DELETE /api/v1/shops/:id`
  ## >>
  ## >>###### headers
  ## >>- Content-Type: application/json
  ## >>
  ## >##### Response
  ## >> `204`
  def destroy
    @shop.destroy
  end

  private

  def set_shop
    @shop = Shop.find params[:id]
  end

  def shop_params
    params.require(:shop)
          .permit(:name, :address, :details)
  end
end
