ShopifyChallengeSchema = GraphQL::Schema.define do
  mutation(Types::MutationType)
  query(Types::QueryType)

  # TODO: enable preloading to avoid n + 1.
  # enable_preloading
end
