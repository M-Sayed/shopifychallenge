Types::LineItemType = GraphQL::ObjectType.define do
  name 'LineItem'
  description 'LineItem'

  field :id, !types.ID
  field :quantity, !types.Int
  field :product_name, !types.String do
    resolve -> (obj, args, ctx) { obj.product.name }
  end

  field :unit_price, !types.Float do
    resolve -> (obj, args, ctx) { obj.product.price }
  end
end
