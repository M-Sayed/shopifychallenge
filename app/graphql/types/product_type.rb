Types::ProductType = GraphQL::ObjectType.define do
  name 'Product'
  description 'Product'

  field :id, !types.ID
  field :name, !types.String
  field :price, !types.Float
end
