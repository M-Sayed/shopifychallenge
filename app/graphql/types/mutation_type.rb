Types::MutationType = GraphQL::ObjectType.define do
  name "Mutation"

  field :createShop, function: Resolvers::CreateShop.new

  field :updateShop, function: Resolvers::UpdateShop.new

  field :deleteShop, function: Resolvers::DeleteShop.new

  field :deleteOrder, function: Resolvers::DeleteOrder.new

  field :createProduct, function: Resolvers::CreateProduct.new

  field :updateProduct, function: Resolvers::UpdateProduct.new

  field :deleteProduct, function: Resolvers::DeleteProduct.new

  field :createLineItem, function: Resolvers::CreateLineItem.new

  field :updateLineItem, function: Resolvers::UpdateLineItem.new

  field :deleteLineItem, function: Resolvers::DeleteLineItem.new

  # field :createOrder, function: Resolvers::CreateOrder.new
  # field :updateOrder, function: Resolvers::UpdateOrder.new
end
