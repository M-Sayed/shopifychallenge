Types::QueryType = GraphQL::ObjectType.define do
  name "Query"

  field :shops, !types[Types::ShopType] do
    description 'Get all shops'

    resolve -> (obj, args, ctx) do
      Shop.all
    end
  end

  field :shop, !Types::ShopType do
    description 'Get Specific shop by id'

    argument :id, !types.Int
    resolve -> (obj, args, ctx) do
      Shop.find_by!(id: args[:id])
    end
  end

  field :products, !types[Types::ProductType] do
    description 'Get all products of a specific shop (shop_id)'
    argument :shop_id, !types.Int

    resolve -> (obj, args, ctx) do
      Product.where(shop_id: args[:shop_id])
    end
  end

  field :product, Types::ProductType do
    description 'Get Specific product of an shop (shop_id) by id'

    argument :shop_id, !types.Int
    argument :id, !types.Int

    resolve -> (obj, args, ctx) do
      shop = Shop.find_by!(id: args[:shop_id])
      shop && shop.products.find_by!(id: args[:id])
    end
  end

  field :orders, !types[Types::OrderType] do
    description 'Get all orders of a specific shop (shop_id)'
    argument :shop_id, !types.Int

    resolve -> (obj, args, ctx) do
      Order.where(shop_id: args[:shop_id])
    end
  end

  field :order, Types::OrderType do
    description 'Get Specific order of an shop (shop_id) by id'

    argument :shop_id, !types.Int
    argument :id, !types.Int

    resolve -> (obj, args, ctx) do
      shop = Shop.find_by!(id: args[:shop_id])
      shop && shop.orders.find_by!(id: args[:id])
    end
  end

  field :line_items, !types[Types::LineItemType] do
    description 'Get all line items of a specific order (order_id)'

    argument :order_id, !types.Int

    resolve -> (obj, args, ctx) do
      LineItem.where(order_id: args[:order_id])
    end
  end

  field :line_item, Types::LineItemType do
    description 'Get Specific line item of an order (order_id) by id'

    argument :order_id, !types.Int
    argument :id, !types.Int

    resolve -> (obj, args, ctx) do
      order = Order.find_by!(id: args[:order_id])
      order && order.line_items.find_by!(id: args[:id])
    end
  end
end
