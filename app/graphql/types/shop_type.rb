Types::ShopType = GraphQL::ObjectType.define do
  name 'Shop'
  description "Shop"

  field :id, !types.ID
  field :name, !types.String
  field :address, !types.String
  field :details, !types.String

  field :products, -> { !types[Types::ProductType] }
  field :orders, -> { !types[Types::OrderType] }
end
