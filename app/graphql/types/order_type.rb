Types::OrderType = GraphQL::ObjectType.define do
  name 'Order'
  description 'Order'

  field :id, !types.ID
  field :total_price, !types.Float

  field :shop, -> { Types::ShopType }
  field :line_items, -> { !types[Types::LineItemType] }
end
