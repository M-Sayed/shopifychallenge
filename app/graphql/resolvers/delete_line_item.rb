class Resolvers::DeleteLineItem < GraphQL::Function
  description "Delete Specific LineItem by id"

  argument :id, !types.Int, 'line_item id'
  argument :order_id, !types.Int, 'order id'

  type types.Boolean

  def call(_obj, args, _ctx)
    order = Order.find_by!(id: args[:order_id])
    line_item = order.line_items.find_by!(id: args[:id])
    line_item.destroy
  rescue ActiveRecord::RecordNotFound => e
    GraphQL::ExecutionError.new(
      "Record Not Found - 404"
    )
  end
end
