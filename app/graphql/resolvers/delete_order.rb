class Resolvers::DeleteOrder < GraphQL::Function
  description "Delete Specific Order by id"
  
  argument :shop_id, !types.Int, "order's shop id"
  argument :id, !types.Int, "order id"

  type types.Boolean

  def call(_obj, args, _ctx)
    shop = Shop.find_by!(id: args[:shop_id])
    order = shop.orders.find_by!(id: args[:id])
    order.destroy
  rescue ActiveRecord::RecordNotFound => e
    GraphQL::ExecutionError.new(
      "Record Not Found - 404"
    )
  end
end
