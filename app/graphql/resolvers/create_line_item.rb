class Resolvers::CreateLineItem < GraphQL::Function
  description "Create New LineItem"

  argument :order_id, !types.Int
  argument :product_id, !types.Int
  argument :quantity, !types.Int

  type Types::LineItemType

  def call(_obj, args, _ctx)
    LineItem.create!(
      order_id: args[:order_id],
      product_id: args[:product_id],
      quantity: args[:quantity]
    )
  rescue ActiveRecord::RecordInvalid => e
    GraphQL::ExecutionError.new(
      "Invalid input: #{e.record.errors.full_messages.join(', ')}"
    )
  end
end
