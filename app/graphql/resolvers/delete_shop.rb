class Resolvers::DeleteShop < GraphQL::Function
  description "Delete Specific Shop by id"
  
  argument :id, !types.Int, 'shop id'

  type types.Boolean

  def call(_obj, args, _ctx)
    shop = Shop.find_by!(id: args[:id])
    shop.destroy
  rescue ActiveRecord::RecordNotFound => e
    GraphQL::ExecutionError.new(
      "Record Not Found - 404"
    )
  end
end
