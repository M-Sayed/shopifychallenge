class Resolvers::DeleteProduct < GraphQL::Function
  description "Delete Specific Product by id"

  argument :shop_id, !types.Int, 'shop id'
  argument :id, !types.Int, 'product id'

  type types.Boolean

  def call(_obj, args, _ctx)
    shop = Shop.find_by!(id: args[:shop_id])
    product = shop.products.find_by!(id: args[:id])
    product.destroy
  rescue ActiveRecord::RecordNotFound => e
    GraphQL::ExecutionError.new(
      "Record Not Found - 404"
    )
  end
end
