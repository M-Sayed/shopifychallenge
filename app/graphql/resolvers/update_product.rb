class Resolvers::UpdateProduct < GraphQL::Function
  description "Update Specific Product by id"

  argument :shop_id, !types.Int, 'shop id'
  argument :id, !types.Int, 'product id'
  argument :name, types.String
  argument :price, types.Float

  type Types::ProductType

  def call(_obj, args, _ctx)
    shop = Shop.find_by!(id: args[:shop_id])
    product = shop.products.find_by!(id: args[:id])

    if product.update(updated_keys(args))
      product
    else
      GraphQL::ExecutionError.new(
        "Invalid input: #{e.record.errors.full_messages.join(', ')}"
      )
    end
  end

  def updated_keys(args)
    {}.tap do |params|
      [:name, :price].each { |f| params[f] = args[f] if args[f] }
    end
  end
end
