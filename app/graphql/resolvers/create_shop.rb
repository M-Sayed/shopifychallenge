class Resolvers::CreateShop < GraphQL::Function
  description "Create New Shop"
  
  argument :name, !types.String
  argument :address, !types.String
  argument :details, !types.String

  type Types::ShopType

  def call(_obj, args, _ctx)
    Shop.create!(
      name: args[:name],
      address: args[:address],
      details: args[:details]
    )
  rescue ActiveRecord::RecordInvalid => e
    GraphQL::ExecutionError.new(
      "Invalid input: #{e.record.errors.full_messages.join(', ')}"
    )
  end
end
