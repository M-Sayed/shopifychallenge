class Resolvers::UpdateShop < GraphQL::Function
  description "Update Specific Shop by id"

  argument :id, !types.Int, 'shop id'
  argument :name, types.String
  argument :address, types.String
  argument :details, types.String

  type Types::ShopType

  def call(_obj, args, _ctx)
    shop = Shop.find_by!(id: args[:id])

    if shop.update(updated_keys(args))
      shop
    else
      GraphQL::ExecutionError.new(
        "Invalid input: #{e.record.errors.full_messages.join(', ')}"
      )
    end
  end

  private
  def updated_keys(args)
    {}.tap do |params|
      [:name, :details, :address].each { |f| params[f] = args[f] if args[f] }
    end
  end
end
