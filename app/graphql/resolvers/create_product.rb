class Resolvers::CreateProduct < GraphQL::Function
  description "Create New Product"

  argument :shop_id, !types.Int
  argument :name, !types.String
  argument :price, !types.Float

  type Types::ProductType

  def call(_obj, args, _ctx)
    Product.create!(
      shop_id: args[:shop_id],
      name: args[:name],
      price: args[:price].try(:to_d).try(:truncate, 2)
    )
  rescue ActiveRecord::RecordInvalid => e
    GraphQL::ExecutionError.new(
      "Invalid input: #{e.record.errors.full_messages.join(', ')}"
    )
  end
end
