class Resolvers::UpdateLineItem < GraphQL::Function
  description "Update Specific LineItem quantity by id"

  argument :order_id, !types.Int, 'order id'
  argument :id, !types.Int, 'line item id'
  argument :quantity, !types.Int

  type Types::LineItemType

  def call(_obj, args, _ctx)
    order = Order.find_by!(id: args[:order_id])
    line_item = order.line_items.find_by!(id: args[:id])

    if line_item.update(quantity: args[:quantity])
      line_item
    else
      GraphQL::ExecutionError.new(
        "Invalid input: #{e.record.errors.full_messages.join(', ')}"
      )
    end
  end
end
