# Shopify Winter 2019 Developer Intern Challenge

## Before you start

- You can find the source code of this project on bitbucket here: https://bitbucket.org/M-Sayed/shopifychallenge/src/master/
- This api is deployed on Heroku, you can test it on https://shopifying.herokuapp.com/api/v1
- The database has already been seeded with: 
      - 5 shops.
      - 10 orders, 2 for each shop.
      - 50 products, 10 for each shop.
      - 29 line_items.

## Setup

**This API has been built using Ruby on Rails.**

To setup the application locally, you will need:

- Ruby 2.4.0
- Rails 5.2.1
- MySQL
- clone the applicaton `git clone https://M-Sayed@bitbucket.org/M-Sayed/shopifychallenge.git`
- install the bundles `bundle install`
- prepare the database `rake db:create db:migrate db:seed`
- run the application `rails server -p5000`
- run the specs `RAILS_ENV=test rspec`
- query the API by curl `curl -H "Content-Type: application/json" -X GET http://localhost:5000/api/v1/shops` or any other tool (i.e. postman).

## API Documentation

Below you will find the docs to use either the API built with GraphQL or with REST.

## GraphQL API

To use GraphQL open the following url:

> http://localhost:5000/graphiql

*You can find the docs of the GraphQL API in the upper right corner.*

## REST API

Before you make any request **You should provide the content type header `Content-Type: application/json`**, otherwise you won't get any valid response.


### Shops Controller APIs
#### Endpoints provided by this controller:
- GET    /api/v1/shops
- POST   /api/v1/shops
- GET    /api/v1/shops/:id
- PATCH  /api/v1/shops/:id
- PUT    /api/v1/shops/:id
- DELETE /api/v1/shops/:id


#### `GET /api/v1/shops/`
*Shops index to list all the shops.*

>##### Request
>>`GET /api/v1/shops/`
>>###### headers
>>- Content-Type: application/json
>
>##### Response
>>###### body
```json
{
  "shops": [{
    "id": 1,
    "name": "shop1",
    "address": "egypt",
    "details": "details",
    "orders": [{
      "id": 1,
      "total_price": "203.74"
    }],
    "products": [{
      "id": 1,
      "name": "Shampoo",
      "price": "19.99"
    }]
  }, {
    "id": 2,
    "name": "shop2",
    "address": "egypt",
    "details": "details",
    "orders": [],
    "products": [{
      "id": 9,
      "name": "blue logitech mouse shop3",
      "price": "30.19"
    }]
  }]
}
```

#### `GET /api/v1/shops/:id`
*Get a specific shop by id.*

>##### Request
>>
>>`GET /api/v1/shops/:id`
>>###### headers
>>- Content-Type: application/json
>>
>##### Response
>>###### body:
```json
{
  "shop": {
    "id": 1,
    "name": "shop1",
    "address": "egypt",
    "details": "details",
    "orders": [{
      "id": 1,
      "total_price": "203.74"
    }],
    "products": [{
      "id": 1,
      "name": "Shampoo",
      "price": "19.99"
    }]
  }
}
```

#### `POST /api/v1/shops`
*Create new shop.*

>##### Request
>>`POST /api/v1/shops`
>>
>>###### headers
>>- Content-Type: application/json
>>
>>###### body:
```json
{
  "shop": {
    "name": "shop1",
    "address": "egypt",
    "details": "details"
  }
}
```
>
>##### Response
>>###### body:
```json
{
  "shop": {
    "id": 1,
    "name": "shop1",
    "address": "egypt",
    "details": "details"
  }
}
```

#### `PUT/PATCH /api/v1/shops/:id`
*Update existing shop by id*

>##### Request
>>`PUT/PATCH /api/v1/shops/:id`
>>
>>###### headers
>>- Content-Type: application/json
>>
>>###### body:
```json
{
  "shop": {
    "name": "shop2",
    "details": "details2"
  }
}
```
>
>##### Response
>>`204`

#### `DELETE /api/v1/shops/:id`
*Delete a specific shop by id*

>##### Request
>>`DELETE /api/v1/shops/:id`
>>
>>###### headers
>>- Content-Type: application/json
>>
>##### Response
>> `204`
* * *
### Products Controller APIs
#### Endpoints provided by this controller:
- GET    /api/v1/shops/:shop_id/products
- GET    /api/v1/shops/:shop_id/orders/:order_id/products
- POST   /api/v1/shops/:shop_id/products
- GET    /api/v1/shops/:shop_id/products/:id
- PATCH  /api/v1/shops/:shop_id/products/:id
- PUT    /api/v1/shops/:shop_id/products/:id
- DELETE /api/v1/shops/:shop_id/products/:id


#### `GET /api/v1/shops/:shop_id/products`
*List all products of a specific shop.*

>##### Request
>>`GET /api/v1/shops/:shop_id/products`
>>###### headers
>>- Content-Type: application/json
>
>##### Response
>>###### body
```json
{
  "products": [{
    "id": 1,
    "name": "Emery Bauch",
    "price": "807.09"
  },
  {
    "id": 2,
    "name": "Dr. Li Ryan",
    "price": "607.04"
  }]
}
```


#### `GET /api/v1/shops/:shop_id/orders/:order_id/products`
*List all products of a specific shop and order.*

>##### Request
>>`GET /api/v1/shops/:shop_id/orders/:order_id/products`
>>###### headers
>>- Content-Type: application/json
>
>##### Response
>>###### body
```json
{
  "products": [{
    "id": 3,
    "name": "Jannet Gislason",
    "price": "504.01"
  },
  {
    "id": 5,
    "name": "Zachery O'Keefe",
    "price": "901.06"
  }]
}
```

#### `GET /api/v1/shops/:shop_id/products/:id`
*Get a specific prodyct of a shop by id.*

>##### Request
>>
>>`GET /api/v1/shops/:shop_id/products/:id`
>>###### headers
>>>- Content-Type: application/json
>>
>##### Response
>>###### body:
```json
{
  "product": {
    "id": 1,
    "name": "Emery Bauch",
    "price": "807.09"
  }
}
```

#### `POST /api/v1/shops/:shop_id/products`
*Create new product for a specific shop.*

>##### Request
>>`POST /api/v1/shops/:shop_id/products`
>>
>>###### headers
>>- Content-Type: application/json
>>
>>###### body:
```json
{
  "product": {
    "name": "Emery Bauch",
    "price": "807.09"
  }
}
```
>
>##### Response
>>###### body:
```json
{
  "product": {
    "id": 1,
    "name": "Emery Bauch",
    "price": "807.09"
  }
}
```

#### `PUT/PATCH /api/v1/shops/:shop_id/products/:id`
*Update existing product by id*

>##### Request
>>`PUT/PATCH /api/v1/shops/:shop_id/products/:id`
>>
>>###### headers
>>- Content-Type: application/json
>>
>>###### body:
```json
{
  "product": {
    "name": "Emery Bauch new",
  }
}
```
>
>##### Response
>> `204`

#### `DELETE /api/v1/shops/:shop_id/products/:id`
*Delete a specific product by id*

>##### Request
>>`DELETE /api/v1/shops/:shop_id/products/:id`
>>
>>###### headers
>>- Content-Type: application/json
>>
>##### Response
>> `204`
* * *
### Orders Controller APIs
#### Endpoints provided by this controller:
- GET    /api/v1/shops/:shop_id/orders
- POST   /api/v1/shops/:shop_id/orders
- GET    /api/v1/shops/:shop_id/orders/:id
- PATCH  /api/v1/shops/:shop_id/orders/:id
- PUT    /api/v1/shops/:shop_id/orders/:id
- DELETE /api/v1/shops/:shop_id/orders/:id


#### `GET /api/v1/shops/:shop_id/orders`
*Orders index to list all the orders of a shop.*

>##### Request
>>`GET /api/v1/shops/:shop_id/orders`
>>###### headers
>>- Content-Type: application/json
>
>##### Response
>>###### body
```json
{
  "orders": [{
    "id": 1,
    "total_price": "4108.25",
    "shop": {
      "id": 1,
      "name": "Shop_0",
      "address": "Apt. 929 3381 Lebsack Oval, South Ian, NC 82844-8016",
      "details": "Est est qui. Ut animi quo. Aliquam nam placeat."
    },
    "line_items": [{
      "id": 1,
      "quantity": 1,
      "unit_price": "504.01",
      "product_name": "Jannet Gislason"
    }, {
      "id": 2,
      "quantity": 4,
      "unit_price": "901.06",
      "product_name": "Zachery O'Keefe"
    }]
  }]
}
```

#### `GET /api/v1/shops/:shop_id/orders/:id`
*Get a specific order of a specific shop by id.*

>##### Request
>>
>>`GET /api/v1/shops/:shop_id/orders/:id`
>>###### headers
>>- Content-Type: application/json
>>
>##### Response
>>###### body:
```json
{
  "order": {
    "id": 1,
    "total_price": "4108.25",
    "shop": {
      "id": 1,
      "name": "Shop_0",
      "address": "Apt. 929 3381 Lebsack Oval, South Ian, NC 82844-8016",
      "details": "Est est qui. Ut animi quo. Aliquam nam placeat."
    },
    "line_items": [{
      "id": 1,
      "quantity": 1,
      "unit_price": "504.01",
      "product_name": "Jannet Gislason"
    }, {
      "id": 2,
      "quantity": 4,
      "unit_price": "901.06",
      "product_name": "Zachery O'Keefe"
    }]
  }
}
```

#### `POST /api/v1/shops/:shop_id/orders`
*Create new order.*

>##### Request
>>`POST /api/v1/shops/:shop_id/orders`
>>
>>###### headers
>>- Content-Type: application/json
>>
>>###### body:
```json
{
  "order": {
    "line_items_attributes": [{
      "product_id": 1,
      "quantity": 2
    }, {
      "product_id": 2,
      "quantity": 4
    }]
  }
}
```
>
>##### Response
>>###### body:
```json
{
  "order": {
    "id": 1,
    "total_price": "4108.25",
    "shop": {
      "id": 1,
      "name": "Shop_0",
      "address": "Apt. 929 3381 Lebsack Oval, South Ian, NC 82844-8016",
      "details": "Est est qui. Ut animi quo. Aliquam nam placeat."
    },
    "line_items": [{
      "id": 1,
      "quantity": 2,
      "unit_price": "504.01",
      "product_name": "Jannet Gislason"
    }, {
      "id": 2,
      "quantity": 4,
      "unit_price": "901.06",
      "product_name": "Zachery O'Keefe"
    }]
  }
}
```

#### `PUT/PATCH /api/v1/shops/:shop_id/orders/:id`
*update existing order by id*

>##### Request
>>`PUT/PATCH /api/v1/shops/:shop_id/orders/:id`
>>
>>###### headers
>>- Content-Type: application/json
>>
>>###### body:
```json
{
  "order": {
    "line_items_attributes": [{
      "product_id": 1,
      "quantity": 3
    }]
  }
}
```
>
>##### Response
>> `204`

#### `DELETE /api/v1/shops/:shop_id/orders/:id`
*Delete a specific order by id*

>##### Request
>>`DELETE /api/v1/shops/:shop_id/orders/:id`
>>
>>###### headers
>>- Content-Type: application/json
>>
>##### Response
>> `204`
* * *
### LineItems Controller APIs
#### Endpoints provided by this controller:
- GET    /api/v1/shops/:shop_id/orders/:order_id/line_items
- POST   /api/v1/shops/:shop_id/orders/:order_id/line_items
- GET    /api/v1/shops/:shop_id/orders/:order_id/line_items/:id
- PATCH  /api/v1/shops/:shop_id/orders/:order_id/line_items/:id
- PUT    /api/v1/shops/:shop_id/orders/:order_id/line_items/:id
- DELETE /api/v1/shops/:shop_id/orders/:order_id/line_items/:id


#### `GET /api/v1/shops/:shop_id/orders/:order_id/line_items`
*LineItems index to list all the line items of an order.*

>##### Request
>>`GET /api/v1/shops/:shop_id/orders/:order_id/line_items`
>>###### headers
>>- Content-Type: application/json
>
>##### Response
>>###### body
```json
{
  "line_items": [{
    "id": 1,
    "quantity": 1,
    "unit_price": "504.01",
    "product_name": "Jannet Gislason"
  }, {
    "id": 2,
    "quantity": 4,
    "unit_price": "901.06",
    "product_name": "Zachery O'Keefe"
  }]
}
```

#### `GET /api/v1/shops/:shop_id/orders/:order_id/line_items/:id`
*Get a specific line item of an order.*

>##### Request
>>
>>`GET /api/v1/shops/:shop_id/orders/:order_id/line_items/:id`
>>###### headers
>>- Content-Type: application/json
>>
>##### Response
>>###### body:
```json
{
  "line_item": {
    "id": 1,
    "quantity": 1,
    "unit_price": "504.01",
    "product_name": "Jannet Gislason"
  }
}
```

#### `POST /api/v1/shops/:shop_id/orders/:order_id/line_items`
*Add new line item to an order.*

>##### Request
>>`POST /api/v1/shops/:shop_id/orders/:order_id/line_items`
>>
>>###### headers
>>- Content-Type: application/json
>>
>>###### body:
```json
{
  "line_item": {
    "product_id": 1,
    "quantity": 2
  }
}
```
>
>##### Response
>>###### body:
```json
{
  "line_item": {
    "id": 1,
    "quantity": 2,
    "unit_price": "504.01",
    "product_name": "Jannet Gislason"
  }
}
```

#### `PUT/PATCH /api/v1/shops/:shop_id/orders/:order_id/line_items/:id`
*update line item quantity of an order.*

>##### Request
>>`PUT/PATCH /api/v1/shops/:shop_id/orders/:order_id/line_items/:id`
>>
>>###### headers
>>- Content-Type: application/json
>>
>>###### body:
```json
{
  "line_item": {
    "quantity": 5
  }
}
```
>
>##### Response
>> `204`

#### `DELETE /api/v1/shops/:shop_id/orders/:order_id/line_items/:id`
*Delete a specific line item by id*

>##### Request
>>`DELETE /api/v1/shops/:id`
>>
>>###### headers
>>- Content-Type: application/json
>>
>##### Response
>> `204`
