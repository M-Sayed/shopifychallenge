source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

# lang & framework
ruby '2.4.0'
gem 'rails', '~> 5.2.1'

# db
gem 'mysql2', '>= 0.4.4', '< 0.6.0'

# app server
gem 'puma', '~> 3.11'

# json serializer
gem 'active_model_serializers', '~> 0.10.0'

# secure api tools.
gem 'rack-cors', require: 'rack/cors'
gem 'rack-attack'

gem 'graphql', '1.7.4'
gem 'graphql-preload'
gem 'graphiql-rails', '1.4.4', group: :development

# deployment
gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

# added to production to seed the database.
gem 'faker', :git => 'https://github.com/stympy/faker.git', :branch => 'master'

group :development, :test do
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'double_doc'
end

group :test do
  gem 'capybara', '~> 2.13'
  gem 'rspec-rails'
  gem 'factory_bot_rails'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
