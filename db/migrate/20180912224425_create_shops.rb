class CreateShops < ActiveRecord::Migration[5.2]
  def change
    create_table :shops do |t|
      t.string :name, null: false
      t.string :address
      t.text :details

      t.timestamps
    end

    add_index :shops, :name, unique: true
    
    reversible do |dir|
      def dir.up
        execute <<-SQL
          ALTER TABLE shops
            ADD CONSTRAINT CHK_empty_name
              CHECK (name <> '');
        SQL
      end

      def dir.down
        execute <<-SQL
          ALTER TABLE shops
            DROP CONSTRAINT CHK_empty_name;
        SQL
      end
    end
  end
end
