class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.references :shop, foreign_key: true, null: false
      t.decimal :total_price, precision: 10,
                              scale: 2,
                              null: false, 
                              default: 0

      t.timestamps
    end
  end
end
