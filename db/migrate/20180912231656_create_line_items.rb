class CreateLineItems < ActiveRecord::Migration[5.2]
  def change
    create_table :line_items do |t|
      t.references :product, foreign_key: true
      t.references :order, foreign_key: true
      t.integer :quantity, null: false

      t.timestamps
    end

    reversible do |dir|
      dir.up do
        execute <<-SQL
          ALTER TABLE line_items
            ADD CONSTRAINT CHK_quantity_valud
              CHECK (quantity > 0);
        SQL
      end
      dir.down do
        execute <<-SQL
          ALTER TABLE line_items
            DROP CONSTRAINT CHK_quantity_valud;
        SQL
      end
    end
  end
end
