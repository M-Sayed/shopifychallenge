5.times do |i|
  shop = Shop.create! name: "Shop_#{i}",
               address: Faker::Address.full_address,
               details: Faker::Lorem.paragraph

  10.times do
    shop.products.create!(
      name: Faker::Name.name,
      price: Faker::Number.decimal(3, 2)
    )
  end

  2.times do
    line_items = []
    cnt = rand(1..4)
    cnt.times do
      line_items << {
        quantity: Faker::Number.between(1, 10),
        product: shop.products.order("RAND()").first
      }
    end
    shop.orders.create(
      line_items_attributes: line_items
    )
  end
end
