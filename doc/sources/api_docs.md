# Shopify Winter 2019 Developer Intern Challenge

## Before you start

- You can find the source code of this project on bitbucket here: https://bitbucket.org/M-Sayed/shopifychallenge/src/master/

- This api is deployed on Heroku, you can test it on https://shopifying.herokuapp.com/api/v1

- The database has already been seeded with
  - 5 shops.
  - 10 orders, 2 for each shop.
  - 50 products, 10 for each shop.
  - 29 line_items.

## Setup

**This API has been built using Ruby on Rails.**

To setup the application locally, you will need:
- Ruby 2.4.0
- Rails 5.2.1
- MySQL
- clone the applicaton `git clone https://M-Sayed@bitbucket.org/M-Sayed/shopifychallenge.git`
- install the bundles `bundle install`
- prepare the database `rake db:create db:migrate db:seed`
- run the application `rails server -p5000`
- run the specs `RAILS_ENV=test rspec`
- query the API by curl `curl -H "Content-Type: application/json" -X GET http://localhost:5000/api/v1/shops` or any other tool (i.e. postman).

## API Documentation

Below you will find the docs to use either the API built with GraphQL or with REST.

## GraphQL API

To use GraphQL open the following url:

> http://localhost:5000/graphiql

*You can find the docs of the GraphQL API in the upper right corner.*

## REST API

Before you make any request **You should provide the content type header `Content-Type: application/json`**, otherwise you won't get any valid response.


@import app/controllers/api/v1/shops_controller.rb
* * *
@import app/controllers/api/v1/products_controller.rb
* * *
@import app/controllers/api/v1/orders_controller.rb
* * *
@import app/controllers/api/v1/line_items_controller.rb
