if Rails.env.development?
  require 'double_doc'

  DoubleDoc::Task.new(
    :double_doc,
    sources:          'doc/sources/*.md',
    md_destination:   'doc/docs'
  )
end
